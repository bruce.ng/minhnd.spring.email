
package common;

import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailMail {
	
	private MailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	private JavaMailSender javamailSender;
	
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}
	
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendMail(String from, String to, String subject, String msg) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(msg);
		mailSender.send(message);
	}
	
	public void sendMail(String dear, String content) {
		
		SimpleMailMessage message = new SimpleMailMessage(simpleMailMessage);
		
		message.setText(String.format(simpleMailMessage.getText(), dear, content));
		
		mailSender.send(message);
		
	}
	
	public void sendMailAttachment(String dear, String content) {
		
		MimeMessage message = javamailSender.createMimeMessage();
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(simpleMailMessage.getSubject());
			helper.setText(String.format(simpleMailMessage.getText(), dear, content));
			
			FileSystemResource file = new FileSystemResource("C:\\log.txt");
			helper.addAttachment(file.getFilename(), file);
			
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		javamailSender.send(message);
	}
	
}
